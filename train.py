from typing import List, Tuple, Iterable, Union, Callable, Dict
from queue import Queue, deque
from operator import add

from IPython import embed
import numpy as np
from scipy.special import expit, logit
import tensorflow as tf
from keras.models import Sequential
from keras.layers import LSTM, Dropout, Activation, Dense
from keras.callbacks import Callback, EarlyStopping

from nltk.tokenize import word_tokenize
from gensim.corpora import Dictionary
from gensim.models import TfidfModel

from word2vecReader import load_word2vec_format
from mongo_interface import MongoInterface
from secrets import mongo_username, mongo_password

#WORD2VEC = load_word2vec_format(fname='./word2vec_twitter_model.bin', binary=True)
#EMBEDDING_DIM = WORD2VEC.layer1_size

class LossHistory(Callback):
	def on_train_begin(self, logs={}):
		self.losses = []

	def on_batch_end(self, batch, logs={}):
		self.losses.append(float(logs.get('loss')))


class BOWCorpus:
	def __init__(self, 
		mi : MongoInterface, 
		corpora_dict : Dictionary):
		
		self.mi = mi
		self.corpora_dict = corpora_dict
		self.num_docs = len(mi)


	def __len__(self):
		return self.num_docs


	def __getitem__(self, i : int):
		entry = self.mi[i]
		tokens = entry['tokens']

		return self.corpora_dict.doc2bow(tokens)

	def __iter__(self):
		for entry in self.mi:
			tokens = entry['tokens']
			yield self.corpora_dict.doc2bow(tokens)


class EmbeddingCorpus:
	def __init__(self, 
		mi : MongoInterface, 
		corpora_dict : Dictionary):
		
		self.mi = mi
		self.corpora_dict = corpora_dict

	def __iter__(self):
		for entry in self.mi:
			tokens = entry['tokens']
			embeddings = calc_embeddings(tokens)
			doc = []
			for token, embedding in zip(tokens, embeddings):
				idx = self.corpora_dict.token2id[token]
				doc.append((idx, embedding))

			yield doc


def create_dictionary(mi : MongoInterface) -> Dictionary:
	"""
	mi : MongoInterface
		Connection to Mongodb collection that stores tokenized tweets.

	Returns 
	tweet_dict: Dictionary
		A bag of words dicitonary of all the tokens in the corpora.
	"""
	tweet_dict = Dictionary([entry['tokens'] for entry in mi])

	return tweet_dict


def idx_to_one_hot(idx : int, vec_len : int) -> np.ndarray:
	"""
	idx : int
		Index of one entry in one hot vector.
	vec_len : int
		Total length of one hot vector.

	Returns
	
	vec : np.ndarray
		One hot vector.

	"""
	if idx >= vec_len:
		raise ValueError("idx must be less than vec_len.")

	vec = np.zeros(vec_len)
	vec[idx] = 1

	return vec


def cosine_similarity(y, y_hat):
	y_norm = tf.sqrt( tf.reduce_sum( tf.square(y) ) )
	y_hat_norm = tf.sqrt( tf.reduce_sum( tf.square(y_hat) ) )

	return tf.reduce_sum( tf.mul(y, y_hat) ) / y_norm / y_hat_norm

def calc_embeddings(tokenized:List[str]) -> List[np.ndarray]:
	embeddings = []
	for token in tokenized:
		if token in WORD2VEC:
			emebedding = WORD2VEC[token]
		else: 
			continue

		emebedding.shape = (1, EMBEDDING_DIM)
		embeddings.append(emebedding)

	return embeddings


def build_model(
	lstm_sizes : List[int], 
	dense_layer_size : int, 
	length_train_sequence : int, 
	dropout_rate : float, 
	loss : Union[str, Callable], 
	optimizer: Union[str, Callable] ) -> Sequential:

	with tf.device('/gpu:1'):
		model = Sequential()

		if len(lstm_sizes) == 1:
			model.add(
				LSTM(output_dim = lstm_sizes[0], 
					input_shape=(length_train_sequence, EMBEDDING_DIM)))
		else:
			model.add(
				LSTM(output_dim = lstm_sizes[0], 
					input_shape=(length_train_sequence, EMBEDDING_DIM), 
					return_sequences=True))

		model.add(Dropout(dropout_rate))

		for lstm_size in lstm_sizes[1:-1]:
			model.add(
				LSTM(output_dim = lstm_size, return_sequences=True))

			model.add(Dropout(dropout_rate))

		if len(lstm_sizes) > 1:
			model.add(
				LSTM(output_dim = lstm_sizes[-1]))
			model.add(Dropout(dropout_rate))

		model.add(
			Dense(output_dim=dense_layer_size, activation='softmax'))
		
		model.compile(optimizer, loss)

	return model

def generate_idx_queue(n_training_examples:int, copies_of_training_data:int, shuffle:bool=True):
	idx_queue = Queue()
	idx_list = []
	for _ in range(copies_of_training_data):
		idxs = list(range(n_training_examples))

		if shuffle:
			np.random.shuffle(idxs)
		
		idx_list += idxs
	
	idx_queue.queue =  deque(idx_list) 

	return idx_queue


def sliding_window(sequence:Iterable, win_size:int, step:int=1):
	"""Returns a generator that will iterate through
	the defined chunks of input sequence.  Input sequence
	must be iterable."""

	if step > win_size:
		raise Exception("**ERROR** step must not be larger than win_size.")
	if win_size > len(sequence):
		raise Exception("**ERROR** win_size must not be larger than sequence length.")

	num_chunks = ( (len(sequence)-win_size) // step ) + 1

	for i in range(0, num_chunks*step, step):
		yield sequence[ i : i+win_size ]

def train_generator(tweet_idxs : Queue,
	tweet_dict : Dictionary, 
	length_train_sequence : int, 
	batch_size : int):
	
	mi = MongoInterface(username=mongo_username, password=mongo_password, ip='localhost', 
		db='twitter', collection='trump_tweets')

	X_queue = []
	y_queue = []

	while True:
		X_batch = []
		y_batch = []
		while len(X_batch) < batch_size:
			if len(X_queue) == 0:
				tweet_idx = tweet_idxs.get()
				tokens = mi[tweet_idx]['tokens']
								
				embeddings = calc_embeddings(tokens)

				if len(embeddings) < length_train_sequence + 1:
					continue

				for n_gram in sliding_window(embeddings[:-length_train_sequence+1], win_size=length_train_sequence):
					X_queue.append(np.vstack(n_gram))

				for unigram in tokens[length_train_sequence:]:
					idx = tweet_dict.token2id[unigram]
					one_hot = idx_to_one_hot(idx, len(tweet_dict))
					y_queue.append(one_hot)

			X_batch.append(X_queue.pop())
			y_batch.append(y_queue.pop())

		X_batch = np.array(X_batch)
		y_batch = np.array(y_batch)

		yield X_batch, y_batch

def vec2word(vec:np.ndarray)-> Tuple[str, float, np.ndarray]:
	vec = vec.flatten()

	word, distance, embedding = WORD2VEC.most_similar(positive=[vec], topn=1)[0] 

	return word, distance, embedding

def create_char_dict(mi : MongoInterface) -> Tuple[Dict, Dict]:
	char_to_int = dict()
	int_to_char = dict()
	i = 0

	for entry in mi:
		for char in entry['text']:
			if char not in char_to_int:
				char_to_int[char] = i
				int_to_char[i] = char
				i += 1

	return char_to_int, int_to_char


if __name__ == '__main__':
	mi = MongoInterface(username=mongo_username, password=mongo_password, ip='localhost', 
		db='twitter', collection='trump_tweets')
	

	char_to_int, int_to_char = create_char_dict(mi)
	total_chars = sum([len(entry['text']) for entry in mi])


	#total_train_tokens = sum([entry['n_tokens'] for entry in mi])
	#tweet_dict = create_dictionary(mi)
	
	#bow_corpus = BOWCorpus(mi, tweet_dict)
	#embedding_corpus = EmbeddingCorpus(mi, tweet_dict)
	
	#bow_tfidf = TfidfModel(bow_corpus)
	#embedding_tfidf = TfidfModel(embedding_corpus)


	nb_worker = 10
	batch_size = 100
	nb_epoch = 100
	length_train_sequence = 2
	samples_per_epoch = (total_train_tokens - length_train_sequence) // batch_size


	lstm_size = [256, 256]
	dense_layer_size = len(char_to_int)
	dropout_rate = 0.1
	optimizer = 'adam'
	loss = 'categorical_crossentropy'

	history = LossHistory()
	early_stopping = EarlyStopping(monitor='loss', min_delta=1e-3, patience=5)


	model = build_model(
		lstm_size, 
		dense_layer_size, 
		length_train_sequence, 
		dropout_rate, 
		loss, 
		optimizer)

	tweet_idxs = generate_idx_queue(
		n_training_examples=len(mi), 
		copies_of_training_data=1000)
	
	train_gen = train_generator(
		tweet_idxs,
		tweet_dict, 
		length_train_sequence, 
		batch_size)
	
	model.fit_generator(train_gen, 
		samples_per_epoch = samples_per_epoch,
		nb_epoch = nb_epoch,
		nb_worker = nb_worker,
		pickle_safe = True,
		callbacks = [history, early_stopping])

	#embed()

	X_trains, y_trains = train_gen.__next__()
	y_hats = model.predict(X_trains)
	words_hat = []
	words = []
	for y_train, y_hat in zip(y_trains, y_hats):
		i = np.argmax(y_hat)
		j = np.argmax(y_train)
		words_hat.append(tweet_dict[i])
		words.append(tweet_dict[j])

