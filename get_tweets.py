from typing import List
import re

import tweepy 
from dateutil import parser
from nltk.tokenize import word_tokenize, RegexpTokenizer
from nltk.corpus import stopwords
from IPython import embed

from mongo_interface import MongoInterface
from secrets import consumer_key, consumer_secret, access_key, access_secret, mongo_username, mongo_password

PUNCTUATION = ([
	'\n', '\r', ' ', '!', '"', "'", '(', ')', '*', 
	',', '-', '.', ':', ';', '?', '[', ']', '_'])

tokenizer = RegexpTokenizer(r'\w+')

STOP_WORDS = stopwords.words()

def get_all_tweets(screen_name):
	#Twitter only allows access to a users most recent 3240 tweets with this method
	
	#authorize twitter, initialize tweepy
	auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
	auth.set_access_token(access_key, access_secret)
	api = tweepy.API(auth)
	
	#initialize a list to hold all the tweepy Tweets
	alltweets = []	
	
	#make initial request for most recent tweets (200 is the maximum allowed count)
	new_tweets = api.user_timeline(screen_name = screen_name,count=200)
	
	#save most recent tweets
	alltweets.extend(new_tweets)
	
	#save the id of the oldest tweet less one
	oldest = alltweets[-1].id - 1
	
	#keep grabbing tweets until there are no tweets left to grab
	while len(new_tweets) > 0:
		print( "getting tweets before {}".format(oldest) )
		
		#all subsiquent requests use the max_id param to prevent duplicates
		new_tweets = api.user_timeline(screen_name = screen_name,count=200,max_id=oldest)
		
		#save most recent tweets
		alltweets.extend(new_tweets)
		
		#update the id of the oldest tweet less one
		oldest = alltweets[-1].id - 1
		
		print("...{} tweets downloaded so far".format(len(alltweets)))

	return alltweets

def get_urls(doc:str) -> List[str]:
	urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', doc)

	return urls

def replace_urls(doc:str, replacement:str) -> str:
	"""
	Removes urls from doc.
	"""
	new_doc = re.sub(
		r'\w+:\/{2}[\d\w-]+(\.[\d\w-]+)*(?:(?:\/[^\s/]*))*', replacement, doc)
	
	return new_doc

def insert_tweets_into_db(mi, all_tweets):
	for tweet in all_tweets:
		entry = tweet._json.copy()
		dt = entry['created_at']
		
		text = entry['text']
		urls = get_urls(text)
		text = replace_urls(text, replacement='')
		
		unstopped_tokens = tokenizer.tokenize(text)

		tokens = []
		for token in unstopped_tokens:
			if token not in STOP_WORDS:
				tokens.append(token)

		entry['_id'] = entry['id']
		entry['cleaned_text'] = text
		entry['urls'] = urls
		entry['created_at'] = parser.parse(dt)
		entry['tokens'] = tokens
		entry['n_tokens'] = len(tokens)
		entry['unstopped_tokens'] = unstopped_tokens
		mi.insert_entry(entry)


if __name__ == '__main__':
	screen_name = 'realDonaldTrump'

	all_tweets = get_all_tweets(screen_name)

	mi = MongoInterface(username=mongo_username, password=mongo_password, ip='localhost', 
		db='twitter', collection='trump_tweets')

	insert_tweets_into_db(mi, all_tweets)
	#embed()

