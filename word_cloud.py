from mongo_interface import MongoInterface
from secrets import mongo_username, mongo_password
from wordcloud import WordCloud
	
mi = MongoInterface(username=mongo_username, password=mongo_password, ip='localhost', 
	db='twitter', collection='trump_tweets')

text = ''
for entry in mi:
	tokens = entry['tokens']
	tokens_str = ' '.join(tokens)
	text += ' '
	text += tokens_str

wc = WordCloud().generate(text)